#!/bin/bash

set -e

SLACK_TOKEN="xoxp-380713652850-656481347366-711744043827-3850be2d69c5b64a80187fb62f9bae49"
SLACK_CHANNEL="builds"
MESSAGE=""
FILE_NAME=""
PATH_TO_FILE=""

function upload() {
    curl https://slack.com/api/files.upload -F token="${SLACK_TOKEN}" -F channels="${SLACK_CHANNEL}" -F title="${MESSAGE}" -F filename="${FILE_NAME}" -F file=@"${PATH_TO_FILE}"
}

function usage() {
    cat <<EOF
Usage: $0 options

options:
    -m  Message.
    -n  File name
    -p  Path of file (required)
EOF
}

# Get version
while getopts m:n:p: arg; do
    case $arg in
    m)  MESSAGE=$OPTARG ;;
    n)  FILE_NAME=$OPTARG ;;
    p)  PATH_TO_FILE=$OPTARG ;;
    esac
done

if [ "${PATH_TO_FILE}" == "" ]; then
    usage
    exit 1
fi

upload